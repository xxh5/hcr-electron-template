const { app, BrowserWindow, Menu, Tray} = require('electron')
const { ipcMain } = require('electron')
const fs = require("fs");
const path = require('path');
let tray: any = null

app.commandLine.appendSwitch("disable-site-isolation-trials")

const appPath = path.join(__dirname, "./project");

function createMenu()
{
  let template = []
  // fs.readdirSync('./project').forEach(f => {
  fs.readdirSync(appPath).forEach((f: string) => {
    let url = `.//${f}/index.html`;
    if(fs.existsSync(url)){
      template.push({ label:f, click:()=>{
        createProject(f);
      }})
    }
  })
  template.push(
    { role: 'quit', type: 'radio' },
  )
  tray.setContextMenu(Menu.buildFromTemplate(template))
}
let win

function createWindow () {
  win = new BrowserWindow({
    width: 1024,
    height: 768,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      contextIsolation: false,
      webSecurity: false,
      allowRunningInsecureContent: true
    } 
  })
  win.loadFile('./index.html')
  win.webContents.openDevTools()
  win.on('closed', () => {
    win = null
  })
}

app.on('ready', ()=>{
  tray = new Tray(path.join(__dirname,'appIcon.png'))
  tray.setToolTip('This is my application.')
  createWindow();
  // createMenu();
  // createProject('Explorer');
})

// 当全部窗口关闭时退出。
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

let winArr = [];
function createProject(project: string) {
  let _win = new BrowserWindow({
    width: 1024,
    height: 768,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
      contextIsolation: false,
      webSecurity: false,
      allowRunningInsecureContent: true
    }
  })
  _win.loadFile(`${appPath}/${project}/index.html`)
  _win.webContents.openDevTools()
  _win.on('closed', () => {
    win = null
  })
}

ipcMain.on('clickProject', (event: any, arg: any)=>{
  createProject(arg);
  event.returnValue = arg
})