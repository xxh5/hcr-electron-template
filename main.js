"use strict";
var _a = require('electron'), app = _a.app, BrowserWindow = _a.BrowserWindow, Menu = _a.Menu, Tray = _a.Tray;
var ipcMain = require('electron').ipcMain;
var fs = require("fs");
var path = require('path');
var tray = null;
app.commandLine.appendSwitch("disable-site-isolation-trials");
var appPath = path.join(__dirname, "./project");
function createMenu() {
    var template = [];
    // fs.readdirSync('./project').forEach(f => {
    fs.readdirSync(appPath).forEach(function (f) {
        var url = ".//" + f + "/index.html";
        if (fs.existsSync(url)) {
            template.push({ label: f, click: function () {
                    createProject(f);
                } });
        }
    });
    template.push({ role: 'quit', type: 'radio' });
    tray.setContextMenu(Menu.buildFromTemplate(template));
}
var win;
function createWindow() {
    console.log('----------------------createWindow');
    win = new BrowserWindow({
        width: 1024,
        height: 768,
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
            contextIsolation: false,
            webSecurity: false,
            allowRunningInsecureContent: true
        }
    });
    win.loadFile('./index.html');
    win.webContents.openDevTools();
    win.on('closed', function () {
        win = null;
    });
}
app.on('ready', function () {
    tray = new Tray(path.join(__dirname, 'appIcon.png'));
    tray.setToolTip('This is my application.');
    createWindow();
    // createMenu();
    // createProject('Explorer');
});
// 当全部窗口关闭时退出。
app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
var winArr = [];
function createProject(project) {
    var _win = new BrowserWindow({
        width: 1024,
        height: 768,
        webPreferences: {
            nodeIntegration: true,
            enableRemoteModule: true,
            contextIsolation: false,
            webSecurity: false,
            allowRunningInsecureContent: true
        }
    });
    _win.loadFile(appPath + "/" + project + "/index.html");
    _win.webContents.openDevTools();
    _win.on('closed', function () {
        win = null;
    });
}
ipcMain.on('clickProject', function (event, arg) {
    createProject(arg);
    event.returnValue = arg;
});
